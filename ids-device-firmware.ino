/**
 * IDS Device Firmware
 *
 *  Created on: 2018.08.16
 *
 */

#define TIME_TO_SLEEP  60         //wakeup every X seconds to report
#define ALARM_MIN_DURATION 10     //time in seconds before toggling off alert
#define LORA_REPORT_TIMER 1       //report every Xsec

//#define USE_DISPLAY
#define USE_SERIAL

#include <SPI.h>
#include <LoRa.h>
#include <math.h>

#ifdef USE_DISPLAY
	#include <Wire.h>
	#include "SSD1306.h"
	#define OLED_SDA  4
	#define OLED_SCL  15
	#define OLED_RST  16
	SSD1306 display(0x3c, OLED_SDA, OLED_SCL);
#endif

#define SCK     5
#define MISO    19
#define MOSI    27
#define SS      18
#define RST     14
#define DI00    26
#define PIR_DATA 35
#define BAND    433E6
#define PABOOST true
#define uS_TO_S_FACTOR 1000000    /* Conversion factor for micro seconds to seconds */

struct DeviceDataStruct {
	char 				header[3];
	//char 				device[6];
	uint64_t    timer;
	float       deviceBattVoltage;
	uint8_t     deviceBattPercent;
	bool        devicePirState;
	bool        deviceAlarmActive;
	bool        deviceLowBatt;
};

DeviceDataStruct state = {
	.header="J7",
	//.device="J7-1",
	.timer=0,
	.deviceBattVoltage=0.00f,
	.deviceBattPercent=100,
	.devicePirState=false,
	.deviceAlarmActive=false,
	.deviceLowBatt=false
};

esp_sleep_wakeup_cause_t wakeup_reason;

ulong lastDrawScreen=0;
ulong lastPirEvent=0;
ulong lastPirRead=0;
ulong lastAdcRead=0;
ulong lastLoraReport=0;
ulong packetCounter=0;

RTC_DATA_ATTR int bootCount = 0;

bool readPIR(){
	if(((millis()-lastPirRead)<1000) && (lastPirRead!=0)){
		return state.devicePirState;
	}
	state.devicePirState = (digitalRead(PIR_DATA));
	lastPirRead=millis();
	return state.devicePirState;
}

void readBatteryVoltage(){
	if(((millis()-lastAdcRead)<5000) && (lastAdcRead!=0)){
		return;
	}
	state.deviceBattVoltage = (analogRead(34)/4095.00f)*2*3.3f*1.1f;
	state.deviceBattPercent = map((int)(state.deviceBattVoltage*1000),3000,4200,0,100);
	state.deviceLowBatt = state.deviceBattPercent<=10;
	lastAdcRead=millis();
}

void setup()
{
	pinMode(PIR_DATA, INPUT);

	wakeup_reason = esp_sleep_get_wakeup_cause();

	#ifdef USE_SERIAL
		Serial.begin(115200);
		Serial.setDebugOutput(true);
		Serial.print("IDS-DEVICE: Starting ! (reason=");
		Serial.print(wakeup_reason);
		Serial.println(")");
	#endif

	SPI.begin(SCK,MISO,MOSI,SS);
	LoRa.setPins(SS,RST,DI00);



	#ifdef USE_DISPLAY
		pinMode(OLED_RST,OUTPUT);
		digitalWrite(OLED_RST, LOW);    // set GPIO16 low to reset OLED
		delay(50);
		digitalWrite(OLED_RST, HIGH); // while OLED is running, must set GPIO16 in high
		display.init();
		display.flipScreenVertically();
		display.setFont(ArialMT_Plain_10);
		display.clear();
		display.drawString(0, 0, "HELLO");
		display.drawString(0, 22, "wakeup_reason");
		display.drawString(90, 22, String(wakeup_reason));
		display.display();
		delay(1500);
		display.clear();
	#endif


	if(bootCount == 0)
		bootCount++;

	if (!LoRa.begin(BAND)){
		#ifdef USE_DISPLAY
			display.drawString(0, 0, "Starting LoRa failed!");
			display.display();
			delay(1000);
		#endif
		ESP.restart();
	}

	readBatteryVoltage();

	if(readPIR()){
		alarmLoop();
		alarmEndLoop();
	}else{
		loraReportState();
		drawScreen();
	}

	delay(2000);

	#ifdef USE_SERIAL
		Serial.println("IDS-DEVICE: going to sleep !");
		Serial.flush();
	#endif

	esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
	esp_sleep_enable_ext0_wakeup(GPIO_NUM_35, 1); //1 = High, 0 = Low
	esp_deep_sleep_start();
}

#ifdef USE_SERIAL
void print64(uint64_t value)
{
	const int NUM_DIGITS    = log10(value) + 1;
	char sz[NUM_DIGITS + 1];
	sz[NUM_DIGITS] =  0;
	for ( size_t i = NUM_DIGITS; i--; value /= 10)
	{
		sz[i] = '0' + (value % 10);
	}

	Serial.print(sz);
}
#endif

void loraReportState() {

	if(((millis()-lastLoraReport)<(LORA_REPORT_TIMER*1000)) && (lastLoraReport>0)){
		return;
	}

	state.timer=(uint64_t)esp_timer_get_time();

	#ifdef USE_SERIAL
		Serial.print("[LORA REPORT]");
		Serial.print(" TS=");
		print64(state.timer);
		Serial.print(" Alarm=");
		Serial.print(String(state.deviceAlarmActive));
		Serial.print(" PIR=");
		Serial.print(String(state.devicePirState));
		Serial.print(" BattVoltage=");
		Serial.print(String(state.deviceBattVoltage));
		Serial.print(" BattPercent=");
		Serial.print(String(state.deviceBattPercent));
		Serial.print(" Lowbatt=");
		Serial.println(String(state.deviceLowBatt));
	#endif

	// send packet
	LoRa.beginPacket(true);
	LoRa.write((const uint8_t*)&state,sizeof(DeviceDataStruct));
	LoRa.endPacket();
	packetCounter++;
	lastLoraReport=millis();
}

void drawScreen(){
	#ifdef USE_DISPLAY
		//24 fps
		if((millis()-lastDrawScreen)<(1000/24)) {
			return;
		}
		display.clear();
		display.drawString(0, 0, "packet:");
		display.drawString(64, 0, String(packetCounter));
		display.drawString(0, 12, "pir:");
		display.drawString(64, 12, String(state.devicePirState));
		display.drawString(0, 24, "alarm:");
		display.drawString(64, 24, String(state.deviceAlarmActive));
		display.drawString(0, 36, "batt:");
		display.drawString(64, 36, String(state.deviceBattVoltage));
		display.drawString(0, 48, "low:");
		display.drawString(64, 48, String(state.deviceLowBatt));
		display.display();
		lastDrawScreen=millis();
	#endif
}

void alarmLoop() {
	#ifdef USE_SERIAL
		Serial.println("> Alarm loop");
	#endif
	state.deviceAlarmActive=true;

	while(state.deviceAlarmActive) {

		readBatteryVoltage();

		if(readPIR()) {
			lastPirEvent=millis();
		}

		state.deviceAlarmActive = (millis()-lastPirEvent)<(ALARM_MIN_DURATION*1000);

		loraReportState();

		#ifdef USE_DISPLAY
		 drawScreen();
		#endif

		delay(200);
	}

	state.deviceAlarmActive = false;
	state.devicePirState = false;
}

void alarmEndLoop(){
	#ifdef USE_SERIAL
		Serial.println("> Alarm end loop");
	#endif
	ulong endAlarmTime = millis();

	state.deviceAlarmActive=false;
	state.devicePirState=false;

	for(int i=0;i<5;i++){
		readBatteryVoltage();
		loraReportState();
		drawScreen();
		delay(1020);
	}
}

//never called
void loop()
{
}
